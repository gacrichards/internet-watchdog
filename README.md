# README #

Pre-Reqs
install git
```
sudo apt install git
```
make repo dir
```
mkdir repo
```
clone repo
```
git clone https://gacrichards@bitbucket.org/gacrichards/internet-watchdog.git
```

Install steps

Initialize submodules
```
git submodule update --init --recursive
```

Install pip
```
sudo apt-get install python-pip
```

Install speedtest command line
```
sudo pip install speedtest-cli
```

Setup mail client for cron to work
```
sudo apt-get install postfix
```
https://askubuntu.com/questions/222512/cron-info-no-mta-installed-discarding-output-error-in-the-syslog

Install requests library
```
pip install requests
```

Setup RPiGPIO
```
pip freeze | grep RPi
```
see what you get after this second command, If you get a valid module for RPi.GPIO or not. And then following if not installed.
```
sudo apt-get install python-dev python-rpi.gpio
```
or
```
pip install RPi.GPIO
```
and then
```
sudo apt-get install picap
picap-setup
```

Customize device id

Open firestore_connection.py

edit the following:
```
device_id = "[MAKE THIS UNIQUE]"
id_url = "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=" + "AIzaSyCjvI5uf35bO2GdPHHL8Cm82MyR4-be1KE"
id_url = "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=" + "AIzaSyCjvI5uf35bO2GdPHHL8Cm82MyR4-be1KE"
login_payload = {"email": "[MAKE THIS UNIQUE]", "password": "helloworld", "returnSecureToken": True}
```

create new user in firestore

run this from /scripts/
```
python firestore_connection.py
```




Test script 
```
/home/pi/repo/internet-watchdog/scripts/speedtest-cron.sh
```

Setup cron job 
```
crontab -e 
*/5 * * * * cd /home/pi/repo/internet-watchdog/scripts && ./speedtest-cron.sh > /home/pi/repo/internet-watchdog/scripts/log/cron.log 2>&1
0 */5 * * * rm /home/pi/repo/internet-watchdog/scripts/id345.txt
```
https://crontab.guru/every-5-minutes
