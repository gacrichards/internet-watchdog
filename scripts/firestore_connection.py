import requests
import os
import keychain as access

device_id = "pi2"
signup_url = "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=" + "AIzaSyCjvI5uf35bO2GdPHHL8Cm82MyR4-be1KE"
id_url = "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=" + "AIzaSyCjvI5uf35bO2GdPHHL8Cm82MyR4-be1KE"
login_payload = {"email": "wifi-bot2@gmail.com", "password": "helloworld", "returnSecureToken": True}
url = 'https://firestore.googleapis.com/v1/projects/internet-watch-258218/databases/(default)/documents/users/' + device_id + '/speed-tests'

def signup():
  rsp = requests.post(signup_url, json=login_payload)
  id_token = rsp.json().get("idToken")
  access.saveAccessToken(id_token)

def login():
  id_token = access.readAccessToken()
  if id_token == None:
    rsp = requests.post(id_url, json=login_payload)
    print(rsp.text)
    id_token = rsp.json().get("idToken")
    access.saveAccessToken(id_token)

  return id_token
  
  
  
  

def postTest(result):
  id_token = login()
  headers = {'Content-type': 'application/json', 'Authorization': "Bearer %s" % id_token  }
  
  payload = {'fields': 
            {'down': {'doubleValue': result['down']},
            'up': {'doubleValue': result['up']},
            'time': {'stringValue': result['time']},
            'ping': {'doubleValue': result['ping']}, 
            'img': {'stringValue': result['img']}
            } 
          }
  params = {'documentId':result['time']}
  print('request')
  print('\theaders: ' + str(headers)) 
  print('\tparams: ' + str(params))
  rsp = requests.post(url, json=payload, headers=headers, params=params)
  if rsp.json().get("error") != None:
    access.deleteAccessToken()
    postTest(result)
  print(rsp.text)

def postReboot(reboot):
  ref = db.collection(u'users').document(user).collection(u'reboots').document(reboot)
  ref.set(reboot)

def main():
    print("hello firestore")
    signup()

if __name__ == '__main__':
  main()