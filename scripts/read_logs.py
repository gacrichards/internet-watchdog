#!/usr/bin/python

# import modules used here -- sys is a very standard one
import sys
import csv
import datetime
import time
import relay_control as relay
# import firebase_connector as fire
import firestore_connection as fire

# Gather our code in a main() function
def main():

  f = open(sys.argv[1], 'rb')
  reader = csv.reader(f)
  rows = []
  for row in reader:
    rows.append(row)
  f.close()
  
  last_test = (rows[len(rows) - 1])
  date_string = last_test[1]
  last_test_date = datetime.datetime.strptime(date_string, "%Y-%m-%d %H:%M:%S")
  now = datetime.datetime.now()
  delta = now - last_test_date
  if delta.seconds > 60:
    print("Failed test - rebooting equipment " + str(now))
    #speed test failed
    relay.reboot(10)
    #we've just rebooted the internet so we need a minute before sending to db
    time.sleep(60)
    fire.postReboot({"time":datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")})
  else:
    json = {}
    json["time"] = date_string
    json["ping"] = last_test[7]
    json["down"] = last_test[8]
    json["up"] = last_test[9]
    json["img"] = last_test[10]
    fire.postTest(json)
    print("Reboot complete")
  
  # Command line args are in sys.argv[1], sys.argv[2] ..
  # sys.argv[0] is the script name itself and can be ignored

# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
  main()