#!/usr/bin/python

# import modules used here -- sys is a very standard one
import sys
import RPi.GPIO as GPIO
import time

def reboot(power_down_time):
  pin = 7
  GPIO.setmode(GPIO.BCM)
  GPIO.setup(pin, GPIO.OUT) 
  GPIO.output(pin, GPIO.HIGH)
  time.sleep(float(power_down_time))
  GPIO.output(pin, GPIO.LOW)
  GPIO.cleanup()

# Gather our code in a main() function
def main():
  reboot()
  
  # Command line args are in sys.argv[1], sys.argv[2] ..
  # sys.argv[0] is the script name itself and can be ignored

# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
  main()