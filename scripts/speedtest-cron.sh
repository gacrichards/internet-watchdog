#!/bin/sh -x

#organizing logs by day
DATE=$(date +%Y/%m/%d)

#path for log files
DIR="/home/pi/repo/internet-watchdog/logs/${DATE}"
# DIR="/Users/colerichards/repos/cole/internet-watchdog/logs/${DATE}"
FILE="${DIR}/logs.csv"
mkdir -p $DIR

# if there is not already a log file create one
[ ! -f ${FILE} ] && touch ${FILE}

# run the speedtest and output the result to our log file
bash -l /home/pi/repo/internet-watchdog/scripts/speedtest-cli-extras/bin/speedtest-csv | tr "\\t" "," >> ${FILE}
# bash -l /Users/colerichards/repos/cole/internet-watchdog/scripts/speedtest-cli-extras/bin/speedtest-csv | tr "\\t" "," >> ${FILE}
#run the python script
python /home/pi/repo/internet-watchdog/scripts/read_logs.py ${FILE}
# python /Users/colerichards/repos/cole/internet-watchdog/scripts/read_logs.py ${FILE}

