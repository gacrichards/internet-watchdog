#!/usr/bin/python

import os

def deleteAccessToken():
    if os.path.exists("id345.txt"):
        os.remove("id345.txt")
        
def readAccessToken():
    try:
        f = open("id345.txt", "r")
        token = f.read()
    except IOError:
        f = open("id345.txt", "w+")
        token = None
    finally:
        f.close()
        return token
        
def saveAccessToken(token):
    f = open("id345.txt", "w")
    f.write(token)
    f.close()

def main():
    print("hello")

# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
  main()