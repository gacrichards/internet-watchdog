#!/usr/bin/python

# import modules used here -- sys is a very standard one
import sys
from firebase import firebase

# db = firebase.FirebaseApplication('https://internet-watchdog.firebaseio.com', None)
db = firebase.FirebaseApplication('https://firestore.googleapis.com/v1/projects/internet-watchdog/databases/test', None)

def updateLastTestResults(json):
  db.patch('last-log', json)

def updateLastReboot(json):
  db.patch('last-reboot', json)

# Gather our code in a main() function
def main():
  print("hello firebase")

  
  # Command line args are in sys.argv[1], sys.argv[2] ..
  # sys.argv[0] is the script name itself and can be ignored

# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
  main()